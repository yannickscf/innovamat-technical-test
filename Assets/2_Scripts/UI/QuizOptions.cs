﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class QuizOptions : MonoBehaviour
{
    [SerializeField] private List<OptionButton> allOptions;

    private bool hasFailed = false;

    private void Start() {
        FillButtonsOptions();
    }

    private void FillButtonsOptions() {
        if (allOptions == null || allOptions.Count == 0) {
            allOptions = new List<OptionButton>();
            allOptions.AddRange(GetComponentsInChildren<OptionButton>());
        }
    }

    public int GetNumberOfOptions() {
        FillButtonsOptions();
        return allOptions.Count;
    }

    /// <summary>
    /// Method to activate sequentially the quiz buttons animations
    /// </summary>
    public void ActivateSequentially() {
        StartCoroutine(ActivateSequentiallyCoroutine());
    }

    /// <summary>
    /// Coroutine to activate sequentially the quiz buttons animations
    /// </summary>
    /// <returns>Coroutine returns</returns>
    private IEnumerator ActivateSequentiallyCoroutine() {
        for (int i = 0; i < allOptions.Count; ++i) {
            allOptions[i].StartAnim();
            yield return new WaitForSeconds(0.5f);
        }
    }

    /// <summary>
    /// Method to set to all buttons their events listener and their
    /// values: the text for the button and if the button is the correct
    /// answer or not
    /// </summary>
    /// <param name="optionsStr">All the answers in random order</param>
    /// <param name="correctOptionIndex">
    /// The index where the answer is located on the array 'optionsStr'
    /// </param>
    public void SetOptionsButtons(string[] optionsStr, int correctOptionIndex) {
        for (int i = 0; i < allOptions.Count; ++i) {
            allOptions[i].OnQuestionResponsed -= OnOptionSelected;
        }

        for (int i = 0; i < allOptions.Count; ++i) {
            allOptions[i].OnQuestionResponsed += OnOptionSelected;
            allOptions[i].SetButtonData(optionsStr[i], i == correctOptionIndex);
        }
    }


    #region Methods to manage quiz button clicks
    /// <summary>
    /// Method subscribed to OptionButton.OnQuestionResponsed, attending to
    /// quiz buttons clicked and adding the correspondant score to the scoreboard
    /// </summary>
    /// <param name="btnPressed">The last option selected</param>
    private void OnOptionSelected(OptionButton btnPressed) {
        GameManager.Instance.SetScoreByCorrectAnswer(btnPressed.IsCorrectAnswer);

        StartCoroutine(BlockButtonsDuringAnimation(btnPressed));

        ManageButtonAnimation(btnPressed);
    }

    /// <summary>
    /// Method to block all other interactions when a button animation
    /// is running
    /// </summary>
    /// <param name="btnPressed">The last option selected</param>
    /// <returns>Coroutine returns</returns>
    private IEnumerator BlockButtonsDuringAnimation(OptionButton btnPressed) {
        for (int i = 0; i < allOptions.Count; ++i) {
            allOptions[i].BlockOrUnblockButton(true);
        }

        yield return new WaitUntil(() => btnPressed.IsReseted());

        for (int i = 0; i < allOptions.Count; ++i) {
            allOptions[i].BlockOrUnblockButton(false);
        }
    }

    /// <summary>
    /// Method to manage the animations of the buttons that haven't been click
    /// </summary>
    /// <param name="btnPressed">The last option selected</param>
    private void ManageButtonAnimation(OptionButton btnPressed) {
        if (btnPressed.IsCorrectAnswer) {
            hasFailed = false;
            StartCoroutine(ExecuteAnimationsOnCorrect(btnPressed));
        } else {
            if (!hasFailed) {
                hasFailed = true;
            } else {
                hasFailed = false;
                StartCoroutine(ExecuteAnimationsOnTooManyFails(btnPressed));
            }
        }
    }

    /// <summary>
    /// Method to execute the corresponding animations when the player
    /// has get the correct answer
    /// </summary>
    /// <param name="btnPressed">The last option selected</param>
    /// <returns>Coroutine returns</returns>
    private IEnumerator ExecuteAnimationsOnCorrect(OptionButton btnPressed) {
        for (int i = 0; i < allOptions.Count; ++i) {
            if (allOptions[i] != btnPressed) {
                allOptions[i].ResetAnimation();
            }
        }

        yield return new WaitUntil(() => allOptions.All(x => x.IsReseted()));
        GameManager.Instance.UpdateGameState(GameState.ShowingNumber);
    }

    /// <summary>
    /// Method to execute the corresponding animations when the player
    /// has commited too many fails
    /// </summary>
    /// <param name="btnPressed">The last option selected</param>
    /// <returns>Coroutine returns</returns>
    private IEnumerator ExecuteAnimationsOnTooManyFails(OptionButton btnPressed) {
        for (int i = 0; i < allOptions.Count; ++i) {
            OptionButton btn = allOptions[i];
            if (btn != btnPressed) {
                if (btn.IsCorrectAnswer)
                    btn.JustShowCorrectAnswer();
                else
                    allOptions[i].ResetAnimation();
            }
        }

        yield return new WaitUntil(() => allOptions.All(x => x.IsReseted()));
        GameManager.Instance.UpdateGameState(GameState.ShowingNumber);
    }
    #endregion
}
