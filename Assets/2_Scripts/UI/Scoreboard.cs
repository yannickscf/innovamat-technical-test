﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Scoreboard : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI successScore;
    [SerializeField] private TextMeshProUGUI wrongScore;

    public void ModifySuccess(int success) {
        successScore.text = success.ToString();
        // Animacion del marcador para aciertos
    }
    public void ModifyFails(int fails) {
        wrongScore.text = fails.ToString();
        // Animacion del marcador para fallos
    }
}
