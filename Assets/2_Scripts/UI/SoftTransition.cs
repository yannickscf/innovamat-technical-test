﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SoftTransition : MonoBehaviour {

    public static event Action<bool> OnTransitionFinished;

    [SerializeField] private Image img;
    
    [SerializeField] private AnimationCurve showCurve;
    [SerializeField] private AnimationCurve unshowCurve;

    [SerializeField] private float transitionSpeed = 1f;


    private void Start() {
        if (img == null)
            img = GetComponent<Image>();
    }

    public void StartHideTransition() {
        if (img != null)
            StartCoroutine(HideTransitionCoroutine());
        else Debug.Log("OJO: que no hay imagen!");
    }

    private IEnumerator HideTransitionCoroutine() {
        img.enabled = true;

        float time = 0f;
        while (img.color.a < 1) {
            time += Time.deltaTime;
            img.color = new Color(Color.black.r, Color.black.g, Color.black.b, showCurve.Evaluate(time * transitionSpeed));
            yield return new WaitForEndOfFrame();
        }
        img.color = new Color(Color.black.r, Color.black.g, Color.black.b, 1);

        OnTransitionFinished?.Invoke(true);
    }

    public void StartRevealTransition() {
        if (img != null)
            StartCoroutine(RevealTransitionCoroutine());
        else Debug.Log("OJO: que no hay imagen!");
    }

    private IEnumerator RevealTransitionCoroutine() {
        yield return new WaitForSeconds(1f);

        float time = 0f;
        while (img.color.a > 0) {
            time += Time.deltaTime;
            img.color = new Color(Color.black.r, Color.black.g, Color.black.b, unshowCurve.Evaluate(time * transitionSpeed));
            yield return new WaitForEndOfFrame();
        }
        img.color = new Color(Color.black.r, Color.black.g, Color.black.b, 0);

        OnTransitionFinished?.Invoke(false);
        img.enabled = false;
    }

}
