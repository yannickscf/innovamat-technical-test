﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NumberToShow : MonoBehaviour {

    [SerializeField] private TextMeshProUGUI numberToShowText;

    private Animator animatorComponent;

    private void Start() {
        animatorComponent = GetComponent<Animator>();
    }

    public void EndAnimation() {
        AnimatorClipInfo[] clipInfo = animatorComponent.GetCurrentAnimatorClipInfo(0);
        AnimatorStateInfo stateInfo = animatorComponent.GetCurrentAnimatorStateInfo(0);
        if (clipInfo[0].clip.name == "NumberToShow_Entry" && stateInfo.speed == -1)
            gameObject.SetActive(false);
    }

    public void SFX_ZoomIn() {
        AnimatorClipInfo[] clipInfo = animatorComponent.GetCurrentAnimatorClipInfo(0);
        AnimatorStateInfo stateInfo = animatorComponent.GetCurrentAnimatorStateInfo(0);
        if (clipInfo[0].clip.name == "NumberToShow_Entry" && stateInfo.speed != -1)
            AudioManager.Instance.PlayZoomIn();
    }

    public void SFX_ZoomOut() {
        AnimatorClipInfo[] clipInfo = animatorComponent.GetCurrentAnimatorClipInfo(0);
        AnimatorStateInfo stateInfo = animatorComponent.GetCurrentAnimatorStateInfo(0);
        if (clipInfo[0].clip.name == "NumberToShow_Entry" && stateInfo.speed == -1)
            AudioManager.Instance.PlayZoomOut();
    }

    public void ChangeText(string newNumber) {
        numberToShowText.text = newNumber;
    }

    public bool IsAnimationFinished() {
        return !gameObject.activeSelf;
    }
}
