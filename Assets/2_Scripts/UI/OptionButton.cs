﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(Button))]
[RequireComponent(typeof(Animator))]
public class OptionButton : MonoBehaviour {

    private Button btnComponent;
    private TextMeshProUGUI textComponent;
    private Animator animatorComponent;

    private bool isCorrectAnswer = false;
    public bool IsCorrectAnswer { get => isCorrectAnswer; }

    public event Action<OptionButton> OnQuestionResponsed;

    private void Init() {
        if (btnComponent == null)
            btnComponent = GetComponent<Button>();
        if (textComponent == null)
            textComponent = GetComponentInChildren<TextMeshProUGUI>();
        if (animatorComponent == null)
            animatorComponent = GetComponent<Animator>();

        ResetAll();
    }

    #region Animations triggers
    public void StartAnim() {
        animatorComponent.SetTrigger("Start");
    }

    public void WrongAnswer() {
        animatorComponent.SetTrigger("Wrong");
    }

    public void CorrectAnswer() {
        animatorComponent.SetTrigger("Correct");
    }

    public void JustShowCorrectAnswer() {
        animatorComponent.SetTrigger("JustShowCorrect");
    }

    public void ResetAnimation() {
        animatorComponent.SetTrigger("Reset");
    }
    #endregion

    /// <summary>
    /// Method to set the values for the button and add the listener
    /// </summary>
    /// <param name="text">Text for the button</param>
    /// <param name="isCorrect">True if is the correct answer for the quiz</param>
    public void SetButtonData(string text, bool isCorrect) {
        Init();

        textComponent.text = text;
        isCorrectAnswer = isCorrect;
        btnComponent.onClick.AddListener(ButtonSelected);
    }

    /// <summary>
    /// Method to execute on button clicked, alerting if
    /// the selection is the correct or not.
    /// </summary>
    private void ButtonSelected() {
        OnQuestionResponsed?.Invoke(this);
        if (isCorrectAnswer) {
            CorrectAnswer();
            AudioManager.Instance.PlayCorrectSound();
        } else {
            WrongAnswer();
            AudioManager.Instance.PlayWrongSound();
        }
    }

    /// <summary>
    /// Reset all the the listener for the button
    /// </summary>
    private void ResetAll() {
        btnComponent.onClick.RemoveAllListeners();
    }

    #region Methods to know the button states
    public bool IsReseted() {
        AnimatorClipInfo[] clipInfo = animatorComponent.GetCurrentAnimatorClipInfo(0);
        return clipInfo[0].clip.name == "Button_Init";
    }

    public void BlockOrUnblockButton(bool block) {
        btnComponent.interactable = !block;
    }
    #endregion
}
