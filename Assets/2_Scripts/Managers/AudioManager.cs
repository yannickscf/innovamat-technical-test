﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : SingletonBehaviour<AudioManager> {

    [Header("General Sounds")]
    [SerializeField] private AudioSource generalSource;
    [SerializeField] private AudioClip backgroundMusic;

    [Header("SFX")]
    [SerializeField] private AudioSource SFXSource;
    [SerializeField] private AudioClip correctAnswer;
    [SerializeField] private AudioClip wrongAnswer;
    [SerializeField] private AudioClip gameStart;
    [SerializeField] private AudioClip zoom;

    private void Start() {
        generalSource.clip = backgroundMusic;
        generalSource.Play();
    }

    #region SFX Sounds players
    public void PlayCorrectSound() {
        SFXSource.pitch = 1f;
        SFXSource.PlayOneShot(correctAnswer);
    }
    public void PlayWrongSound() {
        SFXSource.pitch = 1f;
        SFXSource.PlayOneShot(wrongAnswer);
    }

    public void PlayGameStart() {
        SFXSource.pitch = 1f;
        SFXSource.PlayOneShot(gameStart);
    }

    public void PlayZoomIn() {
        SFXSource.pitch = 0.5f;
        SFXSource.PlayOneShot(zoom);
    }
    public void PlayZoomOut() {
        SFXSource.pitch = 0.5f;
        SFXSource.PlayOneShot(zoom);
    }
    #endregion
}
