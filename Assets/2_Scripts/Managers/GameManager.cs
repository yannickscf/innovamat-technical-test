﻿using System;
using System.Linq;
using UnityEngine;

public enum GameState { Initial, ShowingNumber, SelectingOption, Finished };
public class GameManager : SingletonBehaviour<GameManager> {

    [SerializeField] private int maxNumber = 10;
    [SerializeField] private int numberToShow = 0;

    [SerializeField] private int correctPoints = 0;
    [SerializeField] private int failPoints = 0;

    [SerializeField] private GameState state = GameState.Initial;

    public static event Action<GameState> OnGameStateChanged;

    private void Start() {
        // Init the scoreboard
        UIManager.Instance.ModifyScoreboard(correctPoints, true);
        UIManager.Instance.ModifyScoreboard(failPoints, false);
        // Init randomizer
        UnityEngine.Random.InitState((int)DateTime.Now.Ticks);
        // Init Game
        UpdateGameState(GameState.Initial);
    }

    public void SetScoreByCorrectAnswer(bool isCorrect) {
        // Method that will alert all managers that needd this info
        UIManager.Instance.ModifyScoreboard(isCorrect ? ++correctPoints : ++failPoints, isCorrect);
    }

    /// <summary>
    /// STATE MACHINE for game flow
    /// </summary>
    /// <param name="newState">New state for game flow</param>
    public void UpdateGameState(GameState newState) {
        switch(newState) {
            case GameState.Initial: break;
            case GameState.ShowingNumber:
                string numberText = GetTextNumberToGuess(out numberToShow);
                UIManager.Instance.SetNumberToShow(numberText);
                break;
            case GameState.SelectingOption:
                int numberToShowIndex = 0;
                string[] answers = GetNumbersForAnswers(numberToShow, out numberToShowIndex);
                UIManager.Instance.SetOptions(answers, numberToShowIndex);
                break;
            case GameState.Finished:
                Application.Quit();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        state = newState;
        OnGameStateChanged?.Invoke(newState);
    }

    // Methods to get the random Number
    #region Methods to get number to show
    private string GetTextNumberToGuess(out int number) {
        number = UnityEngine.Random.Range(0, maxNumber);

        return IntToString(number);
    }

    private string IntToString(int number) {
        string res = "Cero";

        switch(number) {
            case 0: res = "Cero"; break;
            case 1: res = "Uno"; break;
            case 2: res = "Dos"; break;
            case 3: res = "Tres"; break;
            case 4: res = "Cuatro"; break;
            case 5: res = "Cinco"; break;
            case 6: res = "Seis"; break;
            case 7: res = "Siete"; break;
            case 8: res = "Ocho"; break;
            case 9: res = "Nueve"; break;
        }

        return res;
    }
    #endregion
    // Methods to get the other possible answers
    #region Methods to get other random numbers
    private string[] GetNumbersForAnswers(int correctNumber, out int correctNumberIndex) {
        string[] res = new string[UIManager.Instance.GetNumberOfOptions()];

        correctNumberIndex = UnityEngine.Random.Range(0, res.Length);

        for (int i = 0; i < res.Length; ++i) {
            if (correctNumberIndex == i) {
                res[i] = correctNumber.ToString();
            } else {
                int newNumber = UnityEngine.Random.Range(0, maxNumber);
                if (newNumber != correctNumber && !res.Any(x => x == newNumber.ToString()))
                    res[i] = newNumber.ToString();
                else
                    i--;
            }
        }

        return res;
    }
    #endregion
}
