﻿using System.Collections;
using UnityEngine;

public class UIManager : SingletonBehaviour<UIManager>
{
    [Header("UI Layouts")]
    [SerializeField] private GameObject startLayout;
    [SerializeField] private GameObject gameLayout;

    [Header("Game UI")]
    [SerializeField] private Scoreboard scoreboard;
    [SerializeField] private NumberToShow numberToShow;
    [SerializeField] private QuizOptions quizOptions;

    [Header("UI Tools")]
    [SerializeField] private SoftTransition transition;



    private void Awake() {
        GameManager.OnGameStateChanged += UpdateUI;
    }

    private void Start() {
        InitializeGameUI();
    }

    private void OnDestroy() {
        GameManager.OnGameStateChanged -= UpdateUI;
    }



    #region UI States management
    /// <summary>
    /// STATE MACHINE for UI Manager
    /// </summary>
    /// <param name="newState">Entry game state</param>
    public void UpdateUI(GameState newState) {
        switch (newState) {
            case GameState.Initial:
                ChangeToGameUI();
                break;
            case GameState.ShowingNumber:
                InitializeGameUI();
                ShowNumberText();
                break;
            case GameState.SelectingOption:
                ShowOptionsToSelect();
                break;
            case GameState.Finished:
            default: break;
        }
    }

    /// <summary>
    /// This methods makes sure that all initial canvas are prepared
    /// to init the game and then reveal it
    /// </summary>
    private void ChangeToGameUI() {
        // Be sure that the initial canvas is showing
        startLayout.SetActive(true);
        gameLayout.SetActive(false);
        // Reveal the game
        transition.StartRevealTransition();
    }

    /// <summary>
    /// Restart the game canvas
    /// </summary>
    private void InitializeGameUI() {
        numberToShow.gameObject.SetActive(false);
        quizOptions.gameObject.SetActive(false);
    }

    /// <summary>
    /// Mthos to start the coroutine that start to show the number
    /// </summary>
    private void ShowNumberText() {
        StartCoroutine(ShowNumberTextCoroutine());
    }

    /// <summary>
    /// Coroutine to Show the number in text format
    /// </summary>
    /// <returns>
    /// This methods waits to end the animation to
    /// change the GameManager state to GameState.SelectingOption
    /// </returns>
    private IEnumerator ShowNumberTextCoroutine() {
        numberToShow.gameObject.SetActive(true);
        yield return new WaitUntil(() => numberToShow.IsAnimationFinished());
        GameManager.Instance.UpdateGameState(GameState.SelectingOption);
    }

    /// <summary>
    /// Method to show all quiz options to select one
    /// </summary>
    private void ShowOptionsToSelect() {
        numberToShow.gameObject.SetActive(false);
        quizOptions.gameObject.SetActive(true);
        quizOptions.ActivateSequentially();
    }
    #endregion


    #region Methods to go from Start Panel to Game Panel
    /// <summary>
    /// Method used by button to start the Game
    /// </summary>
    public void StartButtonPressed() {
        AudioManager.Instance.PlayGameStart();
        SoftTransition.OnTransitionFinished += OnBlackScreen;
        transition.StartHideTransition();
    }

    /// <summary>
    /// Method subscribedd to SoftTransition.OnTransitionFinished to
    /// change the UI layouts from initial to game layout
    /// </summary>
    /// <param name="isBlack">
    /// Entry Parameter of SoftTransition.OnTransitionFinished
    /// </param>
    private void OnBlackScreen(bool isBlack) {
        if (isBlack) {
            startLayout.SetActive(!startLayout.activeSelf);
            gameLayout.SetActive(!gameLayout.activeSelf);

            SoftTransition.OnTransitionFinished += OnTransparentScreen;
            transition.StartRevealTransition();
            SoftTransition.OnTransitionFinished -= OnBlackScreen;
        }
    }

    /// <summary>
    /// Method subscribedd to SoftTransition.OnTransitionFinished to
    /// start the game when transition is finished
    /// </summary>
    /// <param name="isBlack">
    /// Entry Parameter of SoftTransition.OnTransitionFinished
    /// </param>
    private void OnTransparentScreen(bool isBlack) {
        if (!isBlack) {
            GameManager.Instance.UpdateGameState(GameState.ShowingNumber);
            SoftTransition.OnTransitionFinished -= OnTransparentScreen;
        }
    }
    #endregion


    #region Methods to manage the UI interaction from outside
    public void SetNumberToShow(string numberText) {
        numberToShow.ChangeText(numberText);
    }

    public void SetOptions(string[] texts, int correctIndex) {
        quizOptions.SetOptionsButtons(texts, correctIndex);
    }

    public void ModifyScoreboard(int pointsToShow, bool inCorrect) {
        if (inCorrect)
            scoreboard.ModifySuccess(pointsToShow);
        else
            scoreboard.ModifyFails(pointsToShow);
    }
    
    public int GetNumberOfOptions() {
        return quizOptions.GetNumberOfOptions();
    }
    #endregion
}
